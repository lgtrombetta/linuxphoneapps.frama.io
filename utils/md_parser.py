#!/usr/bin/env python3

from common import *

import frontmatter
import re


class MDParser:
    pattern = re.compile(
        r'(### Description(?P<description_long>(.*\n)*))?' +
        r'### Notice(?P<notice>(.*\n?)*)'
        , re.MULTILINE
    )

    @staticmethod
    def load(input_file) -> App:
        post = frontmatter.load(input_file, handler=frontmatter.default_handlers.TOMLHandler())
        match = MDParser.pattern.match(post.content)
        app = App()
        app.title = post['title']
        app.description = post['description']
        if match.group('description_long'):
            app.description_long = match.group('description_long').strip()
        app.date = post['date']
        if 'updated' in post:
            app.updated = post['updated']
        app.notice = match.group('notice').strip()
        # taxonomies
        taxonomies = post['taxonomies']
        app.taxonomies_project_licenses = taxonomies['project_licenses']
        if 'metadata_licenses' in taxonomies:
            app.taxonomies_metadata_licenses = taxonomies['metadata_licenses']
        app.taxonomies_app_author = taxonomies['app_author']
        app.taxonomies_categories = taxonomies['categories']
        app.taxonomies_mobile_compatibility = taxonomies['mobile_compatibility']
        if 'status' in taxonomies:
            app.taxonomies_status = taxonomies['status']
        app.taxonomies_frameworks = taxonomies['frameworks']
        if 'backends' in taxonomies:
            app.taxonomies_backends = taxonomies['backends']
        if 'services' in taxonomies:
            app.taxonomies_services = taxonomies['services']
        if 'packaged_in' in taxonomies:
            app.taxonomies_packaged_in = taxonomies['packaged_in']
        app.taxonomies_freedesktop_categories = taxonomies['freedesktop_categories']
        app.taxonomies_programming_languages = taxonomies['programming_languages']
        app.taxonomies_build_systems = taxonomies['build_systems']
        # extra
        extra = post['extra']
        app.extra_repository = extra['repository']
        if 'homepage' in extra:
            app.extra_homepage = extra['homepage']
        if 'bugtracker' in extra:
            app.extra_bugtracker = extra['bugtracker']
        if 'donations' in extra:
            app.extra_donations = extra['donations']
        if 'translations' in extra:
            app.extra_translations = extra['translations']
        if 'more_information' in extra:
            app.extra_more_information = extra['more_information']
        app.extra_summary_source_url = extra['summary_source_url']
        if 'screenshots' in extra:
            app.extra_screenshots = extra['screenshots']
        if 'app_id' in extra:
            app.extra_app_id = extra['app_id']
        if 'scale_to_fit' in extra:
            app.extra_scale_to_fit = extra['scale_to_fit']
        if 'repology' in extra:
            app.extra_repology = extra['repology']
        if 'appstream_xml_url' in extra:
            app.extra_appstream_xml_url = extra['appstream_xml_url']
        app.extra_reported_by = extra['reported_by']
        if 'updated_by' in extra:
            app.extra_updated_by = extra['updated_by']
        if 'screenshots_img' in extra:
            app.extra_screenshots_img = extra['screenshots_img']
        return app

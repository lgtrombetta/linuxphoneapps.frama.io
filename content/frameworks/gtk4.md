+++
title = "GTK4"
description = "The 4th major release of the GTK toolkit."
date = 2021-08-15T08:50:45+00:00
updated = 2021-08-15T08:50:45+00:00
draft = false
+++


GTK4 is the successor of [GTK+ 3](@/frameworks/gtk3.md). It's often used together with [libadwaita](@/frameworks/libadwaita.md).

* [Website](https://www.gtk.org/)
* [GTK4 Docs](https://docs.gtk.org/gtk4/index.html)
* [Source](https://gitlab.gnome.org/GNOME/gtk/)
+++
title = "Syndic"
description = "Convergent feed reader for Plasma and Android"
aliases = []
date = 2022-09-03
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-3.0",]
app_author = [ "cscarney",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Feed", "News",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/cscarney/syndic"
homepage = "https://github.com/cscarney/syndic"
bugtracker = "https://github.com/cscarney/syndic/issues/"
donations = ""
translations = "https://hosted.weblate.org/engage/syndic/"
more_information = []
summary_source_url = "https://github.com/cscarney/syndic"
screenshots = [ "http://syndic.rocksandpaper.com/screenshots/syndic-narrow.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.rocksandpaper.syndic"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/com.rocksandpaper.syndic"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/cscarney/syndic/master/com.rocksandpaper.syndic.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Syndic is a simple, responsive feed reader made for casual browsing. It adapts to both mouse and touch input, and runs on both desktop and mobile devices. The UI is designed with Plasma Desktop and Android in mind, but it should run in other environments as well.   [Source](https://github.com/cscarney/syndic)
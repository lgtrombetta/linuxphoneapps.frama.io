+++
title = "Pinephone Compass"
description = "This is a proof of concept of a simple compass app for the Pine64 Pinephone."
aliases = []
date = 2021-06-23
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "lgtrombetta",]
categories = [ "compass",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/lgtrombetta/pinephone-compass"
homepage = ""
bugtracker = "https://gitlab.com/lgtrombetta/pinephone-compass/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/lgtrombetta/pinephone-compass"
screenshots = [ "https://gitlab.com/lgtrombetta/pinephone-compass/-/raw/main/data/screenshot.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.lgtrombetta.Compass"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/lgtrombetta/pinephone-compass/-/raw/main/data/com.gitlab.lgtrombetta.Compass.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

For the moment it only supports those hardware versions with the original LIS3MDL magnetometer chip (it was changed circa Q1 2021). The app is written in Python and interfaces with the magnetometer via its Linux Kernel driver. The graphical interface is currently a rudimentary implemetation using matplotlib's plotting capabilities. [Source](https://gitlab.com/lgtrombetta/pinephone-compass)


### Notice

Works great after calibration!
+++
title = "Mumble"
description = "Mumble is a free, open source, low latency, high quality voice chat application."
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "Permissive", "homegrown license",]
metadata_licenses = []
app_author = [ "The Mumble Dev-Team",]
categories = [ "voice chat",]
mobile_compatibility = [ "2",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = [ "Mumble",]
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "Qt", "Network", "Audio",]
programming_languages = [ "C++", "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/mumble-voip/mumble"
homepage = "https://www.mumble.info/"
bugtracker = "https://github.com/mumble-voip/mumble/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://www.mumble.info/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "info.mumble.Mumble"
scale_to_fit = "net.sourgeforge.mumble.mumble"
flathub = "https://flathub.org/apps/details/info.mumble.Mumble"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "mumble",]
appstream_xml_url = "https://raw.githubusercontent.com/mumble-voip/mumble/master/auxiliary_files/config_files/info.mumble.Mumble.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Ok after scale-to-fit
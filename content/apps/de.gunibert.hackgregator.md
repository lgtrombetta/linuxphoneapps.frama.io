+++
title = "Hackgregator"
description = "This application is a comfortable reader application for news.ycombinator.com"
aliases = []
date = 2020-09-23
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Günther Wagner",]
categories = [ "news",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "News",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/gunibert/hackgregator"
homepage = ""
bugtracker = "https://gitlab.com/gunibert/hackgregator/-/issues/"
donations = ""
translations = ""
more_information = [ "https://web.archive.org/web/20220623110901/https://www.gwagner.dev/hackgregator-rewritten-in-rust/",]
summary_source_url = "https://gitlab.com/gunibert/hackgregator"
screenshots = [ "https://gunibert.de/cloud/index.php/s/mXtnzPCf7LtaWGX/preview",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.gunibert.Hackgregator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/de.gunibert.Hackgregator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "hackgregator",]
appstream_xml_url = "https://gitlab.com/gunibert/hackgregator/-/raw/master/data/de.gunibert.Hackgregator.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

This application is a comfortable reader application for news.ycombinator.com. Its intended for the new Librem 5 smartphone from Purism but is not restricted to that. Actually it is a Gtk application usable in every form factor. I try to keep that convergence where its possible. [Source](https://gitlab.com/gunibert/hackgregator)


### Notice

Based on Python and GTK3/libhandy before 0.4.0.
+++
title = "Cuttlefish"
description = "GNOME PeerTube client"
aliases = []
date = 2021-01-23
updated = 2023-01-25

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "artectrex",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "PeerTube",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "AudioVideo", "Player",]
programming_languages = [ "C++",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.shinice.net/artectrex/Cuttlefish"
homepage = ""
bugtracker = "https://gitlab.shinice.net/artectrex/Cuttlefish/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.shinice.net/artectrex/Cuttlefish"
screenshots = [ "https://fosstodon.org/@linmob/105605216244877529",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "ch.cuttlefish.app"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.shinice.net/artectrex/Cuttlefish/-/raw/master/data/ch.cuttlefish.app.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"

+++


### Description

Cuttlefish is a client for PeerTube. PeerTube is a federated video hosting service and uses WebTorrent - a version of BitTorrent that runs in the browser - to help serve videos to users. Cuttlefish is a desktop client for PeerTube, but will work on GNU/Linux-based phones (like the Librem 5 or Pinephone) as well. We want the experience of watching PeerTube videos and using PeerTube in general to be better, by making a native application that will become the best and most efficient way to hook into the federation of interconnected video hosting services. [Source](https://gitlab.shinice.net/artectrex/Cuttlefish)


### Notice

It's very much WIP and seems to be inactive since November 2021.
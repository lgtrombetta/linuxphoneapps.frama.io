+++
title = "signal-rs"
description = "A Rust-based signal app with a QML/Kirigami frontend."
aliases = []
date = 2021-11-18
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "signal-rs",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = [ "presage",]
services = [ "Signal",]
packaged_in = [ "AUR",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Rust", "QML",]
build_systems = [ "cargo",]

[extra]
repository = "https://sr.ht/~nicohman/signal-rs/"
homepage = ""
bugtracker = "https://todo.sr.ht/~nicohman/signal-rs"
donations = ""
translations = ""
more_information = [ "https://teddit.net/r/PINE64official/comments/qthw58/looking_for_some_basic_feedback_on_native_signal/",]
summary_source_url = "https://sr.ht/~nicohman/signal-rs/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "signal-rs",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++


### Description

A Rust-based signal app with a QML/Kirigami frontend. Uses presage as a backend. Better name pending. Many features also still pending. This is definitely a beta version, but I'd love feedback/feature requests. [Source](https://sr.ht/~nicohman/signal-rs/)


### Notice

WIP app which already worked - but Signal likely changed something, so it does not work anymore. In fact, this project may be abandonned: No commits in 9 months.
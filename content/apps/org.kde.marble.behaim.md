+++
title = "Behaim Globus"
description = "Old historic globes"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "geography",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "Debian",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Geography",]
programming_languages = [ "QML", "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/education/marble"
homepage = "https://marble.kde.org/features.php"
bugtracker = "https://invent.kde.org/education/marble/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = [ "https://web.archive.org/web/20190417061449/https://play.google.com/store/apps/details?id=org.kde.marble.behaim",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.marble.behaim"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "marble",]
appstream_xml_url = "https://invent.kde.org/education/marble/-/raw/master/src/apps/behaim/org.kde.marble.behaim.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

The oldest existent globe of the Earth. Martin Behaim and collaborators created the globe around 1492 at the time of Columbus' first sea travel to the west. [Source](https://invent.kde.org/education/marble/-/blob/master/src/apps/behaim/org.kde.marble.behaim.appdata.xml)


### Notice

Part of the debian/pureos package marble-maps, does not come with a launcher though. Launch it with marble-behaim
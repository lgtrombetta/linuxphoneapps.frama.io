+++
title = "Metronome"
description = "A simple Metronome application for the GNOME desktop"
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "a-wai",]
categories = [ "musical tool",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "Debian",]
freedesktop_categories = [ "GTK", "GNOME", "Education", "Music",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://salsa.debian.org/a-wai/gnome-metronome"
homepage = ""
bugtracker = "https://salsa.debian.org/a-wai/gnome-metronome/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/details/com.jvieira.tpt.Metronome"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.jvieira.tpt.Metronome"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-metronome",]
appstream_xml_url = "https://salsa.debian.org/a-wai/gnome-metronome/-/raw/debian/master/data/com.jvieira.tpt.Metronome.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++
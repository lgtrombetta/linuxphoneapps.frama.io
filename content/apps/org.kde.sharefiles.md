+++
title = "Nextcloud-Plasma-Mobile-login"
description = "A test project to create webview login for nextcloud accounts on Plasma Mobile."
aliases = []
date = 2020-03-02
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "cloud syncing",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = [ "Nextcloud",]
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile"
homepage = ""
bugtracker = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile/-/issues/"
donations = ""
translations = ""
more_information = [ "https://orepoala.home.blog/2019/06/11/nextcloud-login-plugin-for-plamo/", "https://community.kde.org/GSoC/2019/StatusReports/ORePoala", "https://summerofcode.withgoogle.com/archive/2019/projects/6343810290810880/",]
summary_source_url = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile"
screenshots = [ "https://orepoala.home.blog/2019/06/11/nextcloud-login-plugin-for-plamo/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.sharefiles"
scale_to_fit = "org.kde.sharefiles"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile/-/raw/master/org.kde.sharefiles.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++



### Notice

Last commit in July 2019.
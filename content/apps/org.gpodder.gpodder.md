+++
title = "GPodder Adaptive"
description = "A mobile friendly adaption of the GPodder GTK UI"
aliases = []
date = 2021-02-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "tree",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed",]
programming_languages = [ "Python",]
build_systems = [ "make",]

[extra]
repository = "https://github.com/gpodder/gpodder/tree/adaptive"
homepage = ""
bugtracker = "https://github.com/gpodder/gpodder/tree/adaptive/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = [ "https://fosstodon.org/@postmarketOS/105676308905800936",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gpodder.gpodder"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gpodder.gpodder-adaptive"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gpodder-adaptive",]
appstream_xml_url = "https://raw.githubusercontent.com/gpodder/gpodder/adaptive/share/metainfo/org.gpodder.gpodder.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Now a branch of the main project, scaling just fine.
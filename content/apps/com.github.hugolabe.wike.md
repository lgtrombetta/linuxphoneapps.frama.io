+++
title = "Wike"
description = "Wikipedia Reader for the GNOME Desktop"
aliases = []
date = 2021-05-09
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Hugo Olabera",]
categories = [ "education",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "GTK", "Education", "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/hugolabe/Wike"
homepage = "https://hugolabe.github.io/Wike/"
bugtracker = "https://github.com/hugolabe/Wike/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/com.github.hugolabe.Wike/",]
summary_source_url = "https://github.com/hugolabe/Wike"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.hugolabe.Wike"
scale_to_fit = "wike"
flathub = "https://flathub.org/apps/details/com.github.hugolabe.Wike"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "wike",]
appstream_xml_url = "https://raw.githubusercontent.com/hugolabe/Wike/master/data/com.github.hugolabe.Wike.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Wike is a Wikipedia reader for the GNOME Desktop. Provides access to all the content of this online encyclopedia in a native application, with a simpler and distraction-free view of articles. [Source](https://github.com/hugolabe/Wike)


### Notice

Standard min-width is a bit too wide, scale-to-fit wike on fixes that.
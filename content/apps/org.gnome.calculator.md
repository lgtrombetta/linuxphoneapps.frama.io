+++
title = "GNOME Calculator"
description = "Calculator for solving mathematical equations"
aliases = []
date = 2021-09-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Calculator",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-calculator"
homepage = "https://wiki.gnome.org/Apps/Calculator"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/issues/"
donations = "https://www.gnome.org/donate/"
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Calculator/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-calculator"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Calculator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnome.Calculator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-calculator",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/master/data/org.gnome.Calculator.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Calculator is an application that solves mathematical equations and is suitable as a default application in a Desktop environment. [Source](https://gitlab.gnome.org/GNOME/gnome-calculator)


### Notice

Adaptive upstream since release 41, GTK4/libadwaita since release 42.
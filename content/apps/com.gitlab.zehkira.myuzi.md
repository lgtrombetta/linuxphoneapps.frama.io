+++
title = "Myuzi"
description = "Stream music from YouTube with no ads."
aliases = []
date = 2022-09-03
updated = 2023-02-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "zehkira",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp", "GStreamer",]
services = [ "YouTube",]
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "GTK", "Audio", "Network", "Player",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://gitlab.com/zehkira/myuzi"
homepage = ""
bugtracker = "https://gitlab.com/zehkira/myuzi/-/issues/"
donations = "https://www.patreon.com/bePatron?u=65739770"
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.zehkira.Myuzi"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/com.gitlab.zehkira.Myuzi"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "myuzi",]
appstream_xml_url = "https://gitlab.com/zehkira/myuzi/-/raw/master/source/data/metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"

+++


### Description

Myuzi is a free and open source music streaming app for Linux. Unlike Spotify, Myuzi: - does not require an account, - has no advertisements, - is completely free, - uses your system GTK theme. All the music is streamed directly from YouTube [Source](https://gitlab.com/zehkira/myuzi)


### Notice

Despite this apps self-description, it is not a [Spotify client](https://linuxphoneapps.org/services/spotify/). It has been archived and been succeded by [Monophony](com.gitlab.zehkira.monophony).
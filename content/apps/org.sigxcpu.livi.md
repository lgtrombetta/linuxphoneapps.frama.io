+++
title = "µPlayer"
description = "A simple GTK4 based video player for mobile phones"
aliases = []
date = 2021-07-19
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Guido Günther",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = [ "gstreamer 1.19",]
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Video", "AudioVideo", "Player",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://source.puri.sm/guido.gunther/livi"
homepage = ""
bugtracker = "https://source.puri.sm/guido.gunther/livi/-/issues/"
donations = ""
translations = ""
more_information = [ "https://social.librem.one/tags/%C2%B5Player",]
summary_source_url = "https://flathub.org/apps/details/org.sigxcpu.Livi"
screenshots = [ "https://social.librem.one/@agx/106561641078758913",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.sigxcpu.Livi"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.sigxcpu.Livi"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://source.puri.sm/guido.gunther/livi/-/raw/main/data/org.sigxcpu.Livi.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Minimalistic video player using GTK4 and GStreamer. The main purpose is to make playing hw accelerated videos with hantro and OpenGL simple. [Source](https://source.puri.sm/guido.gunther/livi)


### Notice

Provides accelerated video playback, also on PinePhone. Needs to be set as default app, as it lacks a dialog to open files.
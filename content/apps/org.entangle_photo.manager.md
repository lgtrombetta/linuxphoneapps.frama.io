+++
title = "Entangle"
description = "Entangle camera control and capture"
aliases = []
date = 2020-09-10
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "The Entangle Photo project",]
categories = [ "camera utility",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "AUR", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Photography",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/entangle/entangle"
homepage = "https://entangle-photo.org/"
bugtracker = "https://gitlab.com/entangle/entangle/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/entangle/entangle"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.entangle_photo.Manager"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.entangle_photo.Manager"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "entangle",]
appstream_xml_url = "https://gitlab.com/entangle/entangle/-/raw/master/src/org.entangle_photo.Manager.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Entangle is a program used to control digital cameras that are connected to the computer via USB. [Source](https://flathub.org/apps/details/org.entangle_photo.Manager)


### Notice

It seems to scale just fine outside of the settings menu, but I do not have a camera to test with scale-to-fit is likely a good idea..
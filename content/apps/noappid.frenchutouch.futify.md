+++
title = "Futify"
description = "Native qml spotify client."
aliases = []
date = 2020-11-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "futify",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "UbuntuComponents",]
backends = []
services = [ "Spotify",]
packaged_in = []
freedesktop_categories = [ "Qt", "Network", "Audio", "Player",]
programming_languages = [ "QML", "Go",]
build_systems = [ "clickable", "go",]

[extra]
repository = "https://gitlab.com/frenchutouch/futify/"
homepage = ""
bugtracker = "https://gitlab.com/frenchutouch/futify/-/issues/"
donations = ""
translations = ""
more_information = [ "https://open-store.io/app/futify.frenchutouch",]
summary_source_url = "https://gitlab.com/frenchutouch/futify/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++
+++
title = "Telegram"
description = "Telegram Desktop messaging app"
aliases = []
date = 2019-02-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "telegramdesktop",]
categories = [ "chat",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = [ "Telegram",]
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "Qt", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/telegramdesktop/tdesktop"
homepage = "https://desktop.telegram.org/"
bugtracker = "https://github.com/telegramdesktop/tdesktop/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/telegramdesktop/tdesktop"
screenshots = [ "https://desktop.telegram.org", "https://mastodon.technology/@kde/101557824824695088",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.telegram.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.telegram.desktop"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "telegram-desktop",]
appstream_xml_url = "https://raw.githubusercontent.com/telegramdesktop/tdesktop/dev/lib/xdg/telegramdesktop.metainfo.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++
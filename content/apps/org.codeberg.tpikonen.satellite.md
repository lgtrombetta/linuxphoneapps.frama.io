+++
title = "Satellite"
description = "Displays navigation satellite (GPS) information from ModemManager enabled devices"
aliases = []
date = 2021-08-02
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "tpikonen",]
categories = [ "geography",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "ModemManager",]
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Geoscience", "Monitor",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://codeberg.org/tpikonen/satellite"
homepage = "https://tpikonen.codeberg.page/satellite/"
bugtracker = "https://codeberg.org/tpikonen/satellite/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/tpikonen/satellite"
screenshots = [ "https://codeberg.org/tpikonen/satellite/raw/branch/main/data/screenshots/screenshot-track.png", "https://codeberg.org/tpikonen/satellite/raw/branch/main/data/screenshots/screenshot1.png", "https://codeberg.org/tpikonen/satellite/raw/branch/main/data/screenshots/screenshot2.png", "https://codeberg.org/tpikonen/satellite/raw/branch/main/data/screenshots/screenshot3.png", "https://codeberg.org/tpikonen/satellite/raw/branch/main/data/screenshots/screenshot4.png", "https://codeberg.org/tpikonen/satellite/raw/branch/main/data/screenshots/screenshot5.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.codeberg.tpikonen.satellite"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/page.codeberg.tpikonen.satellite"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://codeberg.org/tpikonen/satellite/raw/branch/main/data/appdata.xml"
reported_by = "tpikonen"
updated_by = "script"

+++


### Description

Satellite is an adaptive GTK / libhandy application which displays global navigation satellite system (GNSS: GPS et al.) data obtained from modemmanager API. It can also save your position to a GPX-file. [Source](https://codeberg.org/tpikonen/satellite)
+++
title = "Rhythmbox"
description = "Rhythmbox is a music playing application for GNOME."
aliases = []
date = 2020-11-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "music player",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Network", "Feed", "Player",]
programming_languages = [ "C", "Vala",]
build_systems = [ "make",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/rhythmbox"
homepage = "https://wiki.gnome.org/Apps/Rhythmbox"
bugtracker = "https://gitlab.gnome.org/GNOME/rhythmbox/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/rhythmbox/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Rhythmbox3"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "rhythmbox",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/org.gnome.Rhythmbox3.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++
+++
title = "Subsurface"
description = "Subsurface can plan and track single- and multi-tank dives"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "The Subsurface development team",]
categories = [ "sports",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "Qt", "Education", "Geography",]
programming_languages = [ "C++", "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/subsurface/subsurface"
homepage = "https://subsurface-divelog.org"
bugtracker = "https://github.com/subsurface/subsurface/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://subsurface-divelog.org/"
screenshots = [ "https://subsurface-divelog.org/documentation/subsurface-mobile-v2-user-manual/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.subsurface_divelog.Subsurface"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.subsurface_divelog.Subsurface"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "subsurface",]
appstream_xml_url = "https://github.com/subsurface/subsurface/raw/master/appdata/subsurface.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++
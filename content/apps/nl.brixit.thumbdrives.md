+++
title = "Thumbdrives"
description = "Thumbdrive and ISO emulator for phones"
aliases = []
date = 2021-03-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "martijnbraam",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "postmarketOS",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://git.sr.ht/~martijnbraam/thumbdrives"
homepage = ""
bugtracker = "https://todo.sr.ht/~martijnbraam/thumbdrives"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~martijnbraam/thumbdrives"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "nl.brixit.Thumbdrives"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "thumbdrives",]
appstream_xml_url = "https://git.sr.ht/~martijnbraam/thumbdrives/tree/master/item/data/nl.brixit.Thumbdrives.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++
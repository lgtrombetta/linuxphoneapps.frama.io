+++
title = "pass-manager-compact"
description = "Compact GUI for pass to be useable for smartphones"
aliases = []
date = 2021-02-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "chrisu281080",]
categories = [ "password manager",]
mobile_compatibility = [ "needs testing",]
status = []
frameworks = [ "GTK3",]
backends = [ "pass",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]

[extra]
repository = "https://gitlab.com/chrisu281080/pass-manager-compact"
homepage = ""
bugtracker = "https://gitlab.com/chrisu281080/pass-manager-compact/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/chrisu281080/pass-manager-compact"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

#helpwanted, testing needed
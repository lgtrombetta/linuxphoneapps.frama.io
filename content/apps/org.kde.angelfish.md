+++
title = "Angelfish Webbrowser"
description = "Web browser for Plasma Mobile"
aliases = []
date = 2019-02-01
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Flathub",]
freedesktop_categories = [ "Network", "KDE", "Qt", "WebBrowser",]
programming_languages = [ "QML", "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/plasma-mobile/angelfish"
homepage = ""
bugtracker = "https://invent.kde.org/plasma-mobile/angelfish/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/plasma-mobile/angelfish"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.angelfish"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.kde.angelfish"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "angelfish", "kde5-angelfish",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/angelfish/-/raw/master/org.kde.angelfish.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++
+++
title = "WatchMate"
description = "PineTime smart watch companion app for Linux phone and desktop"
aliases = []
date = 2022-09-17
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Andrii Zymohliad",]
categories = [ "watch companion",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://gitlab.com/azymohliad/watchmate"
homepage = ""
bugtracker = "https://gitlab.com/azymohliad/watchmate/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/azymohliad/watchmate"
screenshots = [ "https://gitlab.com/azymohliad/watchmate/uploads/a2b49a69432d3b757fef9d5858c8e787/collage_2022-08-14.png", "https://gitlab.com/azymohliad/watchmate/uploads/db3afec736de83de622ac88ab79ea0c0/dashboard_2022-08-14.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.gitlab.azymohliad.watchmate/1.png", "https://img.linuxphoneapps.org/io.gitlab.azymohliad.watchmate/2.png", "https://img.linuxphoneapps.org/io.gitlab.azymohliad.watchmate/3.png", "https://img.linuxphoneapps.org/io.gitlab.azymohliad.watchmate/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.gitlab.azymohliad.WatchMate"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/io.gitlab.azymohliad.WatchMate"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "watchmate",]
appstream_xml_url = "https://gitlab.com/azymohliad/watchmate/-/raw/main/assets/io.gitlab.azymohliad.WatchMate.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Companion app for PineTime smart watch running InfiniTime firmware. Visually optimized for GNOME, adaptive for Linux phone and desktop. Features: - Serve current time to the watch, - Read various data from the watch, - Perform OTA firmware update, - Automatically check for available firmware updates, - Media player control [Source](https://gitlab.com/azymohliad/watchmate)
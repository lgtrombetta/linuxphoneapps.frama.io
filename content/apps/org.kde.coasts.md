+++
title = "Coast"
description = "A nice and simple map viewer"
aliases = []
date = 2022-04-26
updated = 2023-02-24

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "fhek",]
categories = [ "maps and navigation",]
mobile_compatibility = [ "5",]
status = [ "gone",]
frameworks = [ "Kirigami",]
backends = []
services = [ "openstreetmap",]
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility", "Maps",]
programming_languages = [ "QML", "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/fhek/coast"
homepage = ""
bugtracker = "https://invent.kde.org/fhek/coast/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/fhek/coast"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.kde.coasts/1.png", "https://img.linuxphoneapps.org/org.kde.coasts/2.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.coasts"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/fhek/coast/-/raw/master/org.kde.coast.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"

+++


### Description

A nice and simple map viewer. Features: Navigate to anywhere in the world using the search function [Source](https://invent.kde.org/fhek/coast)


### Notice

Just a simple map viewer, no navigation functionality. Apparently, this project has been deleted and is simply gone, with no public forks surviving.
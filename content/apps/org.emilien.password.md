+++
title = "Password for GNOME"
description = "Password is a password calculator and random generator for Gnome written in Gtk/Vala."
aliases = []
date = 2020-11-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Emilien Lescoute",]
categories = [ "password generator",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/elescoute/password-for-gnome"
homepage = ""
bugtracker = "https://github.com/elescoute/password-for-gnome/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/details/org.emilien.Password"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.emilien.Password"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.emilien.Password"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "password-for-gnome-vala",]
appstream_xml_url = "https://raw.githubusercontent.com/elescoute/password-for-gnome/master/data/org.emilien.Password.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++
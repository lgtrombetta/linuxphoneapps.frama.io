+++
title = "gedit"
description = "Legendary text editor since 1998"
aliases = []
date = 2020-12-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "C", "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gedit"
homepage = "https://wiki.gnome.org/Apps/Gedit"
bugtracker = "https://gitlab.gnome.org/GNOME/gedit/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gedit/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.gedit"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnome.gedit"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gedit",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gedit/-/raw/master/data/org.gnome.gedit.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

gedit is a general-purpose text editor. It has been created in 1998, at the beginnings of GNOME. The first goal of gedit is to be easy to use, with a simple interface by default. More advanced features are available by enabling plugins. [Source](https://gitlab.gnome.org/GNOME/gedit)
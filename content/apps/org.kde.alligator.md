+++
title = "Alligator"
description = "Alligator is a convergent RSS/Atom feed reader."
aliases = []
date = 2020-08-25
updated = 2023-02-16

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Flathub",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Feed", "News",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/network/alligator"
homepage = "https://apps.kde.org/alligator"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Alligator"
donations = "https://kde.org/community/donations/"
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/network/alligator"
screenshots = [ "https://cdn.kde.org/screenshots/alligator/alligator-mobile.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.alligator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.kde.alligator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "alligator", "kde5-alligator",]
appstream_xml_url = "https://invent.kde.org/network/alligator/-/raw/master/org.kde.alligator.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"

+++
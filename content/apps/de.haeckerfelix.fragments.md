+++
title = "Fragments"
description = "A BitTorrent Client."
aliases = []
date = 2019-03-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Felix Häcker",]
categories = [ "bittorrent client",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "BitTorrent",]
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "P2P",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/Fragments"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/Fragments/-/issues/"
donations = "https://liberapay.com/haecker-felix"
translations = "https://l10n.gnome.org/module/Fragments/"
more_information = [ "https://puri.sm/posts/fragments-app-for-the-librem-5/", "https://apps.gnome.org/app/de.haeckerfelix.Fragments/", "https://blogs.gnome.org/haeckerfelix/2022/02/07/the-road-to-fragments-2-0/",]
summary_source_url = "https://gitlab.gnome.org/World/Fragments"
screenshots = [ "https://gitlab.gnome.org/World/Fragments/-/tree/main/data/screenshots", "https://puri.sm/posts/fragments-app-for-the-librem-5/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.haeckerfelix.Fragments"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/de.haeckerfelix.Fragments"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "fragments",]
appstream_xml_url = "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/de.haeckerfelix.Fragments.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Fragments is an easy to use BitTorrent client for the GNOME desktop environment. It is usable for receiving files using the BitTorrent protocol, which enables you to transmit huge files, like videos or installation images for Linux distributions. [Source](https://gitlab.gnome.org/World/Fragments)


### Notice

Ported to GTK4/libadwaita for release 2.0
+++
title = "Discover"
description = "Kirigami-based software center"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "software center",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/plasma/discover"
homepage = ""
bugtracker = "https://invent.kde.org/plasma/discover/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.plasma.discover"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "plasma-discover",]
appstream_xml_url = "https://invent.kde.org/plasma/discover/-/raw/master/discover/org.kde.discover.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++
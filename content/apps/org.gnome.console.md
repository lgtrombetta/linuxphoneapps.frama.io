+++
title = "Console"
description = "A simple user-friendly terminal emulator for the GNOME desktop."
aliases = []
date = 2020-10-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian",]
freedesktop_categories = [ "GTK", "GNOME", "System", "TerminalEmulator",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/console"
homepage = ""
bugtracker = "https://gitlab.gnome.org/GNOME/console/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/console"
screenshots = [ "https://gitlab.gnome.org/GNOME/console/raw/HEAD/data/screenshots/01-Terminal.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Console"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kgx", "gnome-console",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/console/-/raw/main/data/org.gnome.Console.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Console is supposed to be a simple terminal emulator for the average user to carry out simple cli tasks and aims to be a 'core' app for GNOME/Phosh. We are not however trying to replace GNOME Terminal/Tilix, these advanced tools are great for developers and administrators, rather Console aims to serve the casual linux user who rarely needs a terminal. [Source](https://gitlab.gnome.org/GNOME/console)


### Notice

Console was previously called KingsCross / kgx. From GNOME 42 onwards, this is GNOME's default terminal.
+++
title = "QiFlora"
description = "Mobile friendly application to monitor Mi Flora devices."
aliases = []
date = 2019-11-08
updated = 2023-03-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "eyecreate",]
categories = [ "smart home",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://git.eyecreate.org/eyecreate/qiflora"
homepage = ""
bugtracker = "https://git.eyecreate.org/eyecreate/qiflora/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.eyecreate.org/eyecreate/qiflora"
screenshots = [ "https://git.eyecreate.org/eyecreate/qiflora/raw/master/packaging/main_window.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.eyecreate.qiflora"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.eyecreate.qiflora"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://git.eyecreate.org/eyecreate/qiflora/-/raw/master/packaging/org.eyecreate.qiflora.appdata.xml"
reported_by = "eyecreate"
updated_by = "linmob"

+++


### Description

Mobile friendly application to monitor Mi Flora devices. Plants using this device will have th[ei]r data logged when app is used and graphed to better monitor plant health. [Source](https://git.eyecreate.org/eyecreate/qiflora)
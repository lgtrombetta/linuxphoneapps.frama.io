+++
title = "Sublime Music"
description = "A native Revel/Gonic/Subsonic/Airsonic/*sonic client for Linux. Built using Python and GTK+"
aliases = []
date = 2020-10-15
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "sublime-music",]
categories = [ "audio streaming",]
mobile_compatibility = [ "3",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = [ "Subsonic", "ampache", "Revel", "Airsonic", "Gonic", "Navidrome",]
packaged_in = [ "AUR", "postmarketOS", "Debian",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://gitlab.com/sublime-music/sublime-music"
homepage = "https://sublimemusic.app/"
bugtracker = "https://gitlab.com/sublime-music/sublime-music/-/issues/"
donations = ""
translations = ""
more_information = [ "https://sublime-music.gitlab.io/sublime-music/",]
summary_source_url = "https://gitlab.com/sublime-music/sublime-music"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "app.sublimemusic.SublimeMusic"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "sublime-music",]
appstream_xml_url = "https://gitlab.com/sublime-music/sublime-music/-/raw/master/sublime-music.metainfo.xml"
reported_by = "ula"
updated_by = "script"

+++


### Description

Desktop music player for *sonic server and compatible [Source](https://gitlab.com/sublime-music/sublime-music)


### Notice

Scale-to-fit required and switch from portrait to landscape display to access certain menus. Works but interface not designed for a small screen. There is no .desktop, you have to run the command or create the .desktop.
+++
title = "Valent"
description = "Connect, Control and Sync Devices"
aliases = []
date = 2022-06-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Andy Holmes",]
categories = [ "connectivity",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "KDE Connect",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/andyholmes/valent"
homepage = "https://valent.andyholmes.ca/"
bugtracker = "https://github.com/andyholmes/valent/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/andyholmes/valent"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/ca.andyholmes.valent/1.png", "https://img.linuxphoneapps.org/ca.andyholmes.valent/2.png", "https://img.linuxphoneapps.org/ca.andyholmes.valent/3.png", "https://img.linuxphoneapps.org/ca.andyholmes.valent/4.png", "https://img.linuxphoneapps.org/ca.andyholmes.valent/5.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "ca.andyholmes.Valent"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/andyholmes/valent/main/data/metainfo/ca.andyholmes.Valent.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Securely connect your devices to open files and links where you need them, get notifications when you need them, stay in control of your media and more. Features: - Sync contacts, notifications and clipboard content, - Control media players and volume, - Share files, links and text, - Virtual touchpad and keyboard, - Call and text notification, - Execute custom commands. Valent is an implementation of the KDE Connect protocol, built on GNOME platform libraries. [Source](https://github.com/andyholmes/valent)


### Notice

The app is already connecting with KDE Connect (on KDE), can ring and send and receive files. On the date of addition, other functionality could not be tested successfully. Get the app on its [website as a flatpak](https://www.andyholmes.ca/valent/)!
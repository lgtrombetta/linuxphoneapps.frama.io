+++
title = "Dino"
description = "Modern XMPP (\"Jabber\") Chat Client using GTK+/Vala"
aliases = []
date = 2021-01-19
updated = 2023-02-10

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Dino Development Team",]
categories = [ "chat",]
mobile_compatibility = [ "4",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "XMPP",]
packaged_in = [ "postmarketOS",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Vala",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/dino/dino/tree/feature/handy"
homepage = "https://dino.im"
bugtracker = "https://github.com/dino/dino/tree/feature/handy/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.postmarketos.org/wiki/Dino", "https://github.com/dino/dino/issues/178", "https://wiki.mobian.org/doku.php?id=dino",]
summary_source_url = "https://github.com/dino/dino"
screenshots = [ "https://fosstodon.org/@linmob/105583488991640393",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "im.dino.Dino"
scale_to_fit = "dino"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "dino-im",]
appstream_xml_url = "https://raw.githubusercontent.com/dino/dino/feature/handy/main/data/im.dino.Dino.appdata.xml.in"
reported_by = "esu23"
updated_by = "linmob"

+++


### Description

Dino is a modern open-source chat client for the desktop. It focuses on providing a clean and reliable Jabber/XMPP experience while having your privacy in mind. [Source](https://dino.im/)


### Notice

GTK4/libadwaita since release 0.4. The main window adapts perfectly, but account and conversation settings do not (can be fixed on Phosh with scale-to-fit dino on), and the [emoji picker does not work well yet](https://github.com/dino/dino/issues/1360). A flatpak build is available on Flathub's beta branch. For earlier releases, the [feature/handy branch](https://github.com/dino/dino/tree/feature/handy) had good mobile compatibility.
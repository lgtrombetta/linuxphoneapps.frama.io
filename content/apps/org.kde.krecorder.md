+++
title = "Krecorder"
description = "Audio recorder for Plasma Mobile and other platforms"
aliases = []
date = 2020-02-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Plasma Mobile Developers",]
categories = [ "audio recorder",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS",]
freedesktop_categories = [ "Qt", "KDE", "Audio", "Recorder",]
programming_languages = [ "QML", "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/krecorder"
homepage = ""
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=krecorder"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#recorder",]
summary_source_url = "https://invent.kde.org/utilities/krecorder"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.krecorder"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "krecorder", "kde5-krecorder",]
appstream_xml_url = "https://invent.kde.org/utilities/krecorder/-/raw/master/org.kde.krecorder.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

A convergent audio recording application for Plasma. Features: Record audio with a visualizer, and pausing functionality, Ability to select audio sources, Ability to select encoding and container formats, Audio playback with a visualizer. [Source](https://invent.kde.org/utilities/krecorder)
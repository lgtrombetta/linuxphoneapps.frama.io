+++
title = "Communicator"
description = "Maui Dialer and Contacts Manager"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "maui",]
categories = [ "contacts",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS",]
freedesktop_categories = [ "Qt", "Office", "ContactManagement",]
programming_languages = [ "QML", "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/communicator"
homepage = "https://mauikit.org/apps/communicator/"
bugtracker = "https://invent.kde.org/maui/communicator/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/communicator"
screenshots = [ "https://medium.com/nitrux/maui-kde-fcdc920138e2",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.communicator"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "communicator",]
appstream_xml_url = "https://invent.kde.org/maui/communicator/-/raw/master/org.kde.communicator.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++
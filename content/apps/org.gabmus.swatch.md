+++
title = "Swatch"
description = "Color palette manager."
aliases = []
date = 2022-03-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gabriele Musco",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GabMus/swatch"
homepage = ""
bugtracker = "https://gitlab.gnome.org/GabMus/swatch/-/issues/"
donations = "https://liberapay.com/gabmus/donate"
translations = ""
more_information = [ "https://gabmus.org/posts/swatch_a_color_palette_manager/",]
summary_source_url = "https://gitlab.gnome.org/GabMus/swatch"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gabmus.swatch"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gabmus.swatch"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/GabMus/swatch/-/raw/master/data/org.gabmus.swatch.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++
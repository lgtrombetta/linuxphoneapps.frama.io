+++
title = "GNOME Web (Epiphany)"
description = "A simple, clean, beautiful view of the Web"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "The GNOME Project",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "WebBrowser",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/epiphany"
homepage = "https://wiki.gnome.org/Apps/Web"
bugtracker = "https://gitlab.gnome.org/GNOME/epiphany/-/issues/"
donations = ""
translations = ""
more_information = [ "https://puri.sm/posts/end-of-year-librem-5-update/", "https://apps.gnome.org/app/org.gnome.Epiphany/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/epiphany"
screenshots = [ "https://wiki.gnome.org/Apps/Web",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Epiphany"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnome.Epiphany"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "epiphany",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/epiphany/-/raw/master/data/org.gnome.Epiphany.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

GNOME Web (codename: Epiphany) is a GNOME web browser based on the WebKit rendering engine. The codename means "a usually sudden manifestation or perception of the essential nature or meaning of something" (Merriam-Webster). [Source](https://gitlab.gnome.org/GNOME/epiphany)
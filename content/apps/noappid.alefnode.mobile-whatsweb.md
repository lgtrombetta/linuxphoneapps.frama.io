+++
title = "WhatsWeb"
description = "WhatsWeb [WhatsApp Web] for Mobile Linux Devices"
aliases = []
date = 2022-03-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "alefnode",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "Electron",]
backends = []
services = [ "WhatsApp Web",]
packaged_in = []
freedesktop_categories = [ "Network", "Chat", "InstantMessaging",]
programming_languages = [ "JavaScript",]
build_systems = [ "npm",]

[extra]
repository = "https://github.com/alefnode/mobile-whatsweb"
homepage = ""
bugtracker = "https://github.com/alefnode/mobile-whatsweb/issues/"
donations = ""
translations = ""
more_information = [ "https://twitter.com/hadrianweb/status/1507362672446259205",]
summary_source_url = "https://github.com/alefnode/mobile-whatsweb"
screenshots = [ "https://nitter.net/pic/media%2FFOspPm7XsAIRpJr.jpg", "https://nitter.net/pic/media%2FFOspReOWUAEu2pI.jpg",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++


### Description

WhatsApp Electron is an unofficial WhatsApp desktop client using Electron to wrap WhatsApp Web. While it should work on Windows, Mac and Linux, development was targeted for Linux.  We also create a JS that make webpage responsive. [Source](https://github.com/alefnode/mobile-whatsweb)


### Notice

Binary releases available on releases page: https://github.com/alefnode/mobile-whatsweb/releases/tag/v0.2.3
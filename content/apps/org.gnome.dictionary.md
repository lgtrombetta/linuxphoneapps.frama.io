+++
title = "Dictionary"
description = "Look up words in dictionary sources."
aliases = []
date = 2020-10-15
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-2.0-or-later", "LGPL-2.0-or-later", "GFDL-1.3-only",]
metadata_licenses = []
app_author = [ "Emmanuele Bassi",]
categories = [ "dictionary",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "AUR", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "TextTools", "Dictionary",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/Archive/gnome-dictionary"
homepage = "https://wiki.gnome.org/Apps/Dictionary"
bugtracker = "https://gitlab.gnome.org/Archive/gnome-dictionary/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/Archive/gnome-dictionary"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Dictionary"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnome.Dictionary"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-dictionary",]
appstream_xml_url = "https://gitlab.gnome.org/Archive/gnome-dictionary/-/raw/master/data/appdata/org.gnome.Dictionary.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

GNOME Dictionary is a simple, clean, elegant application to look up words in online dictionaries using the DICT protocol. [Source](https://gitlab.gnome.org/Archive/gnome-dictionary)
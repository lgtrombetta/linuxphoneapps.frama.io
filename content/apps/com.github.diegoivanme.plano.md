+++
title = "Plano"
description = "A Cartesian Plane Calculator"
aliases = []
date = 2021-09-28
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "diegoivanme",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Calculator",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/Diego-Ivan/plano-rewritten"
homepage = ""
bugtracker = "https://github.com/Diego-Ivan/plano-rewritten/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Diego-Ivan/plano-rewritten"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.diegoivanme.plano"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://github.com/Diego-Ivan/plano-rewritten/raw/main/data/com.github.diegoivanme.plano.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++
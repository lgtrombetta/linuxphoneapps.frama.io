+++
title = "Plasma Mobile Settings"
description = "Settings application for Plasma Mobile."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "Plasma Mobile Developers",]
categories = [ "settings",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "postmarketOS",]
freedesktop_categories = [ "Qt", "KDE", "Settings", "DesktopSettings",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/plasma-mobile/plasma-settings"
homepage = ""
bugtracker = "https://invent.kde.org/plasma-mobile/plasma-settings/-/issues/"
donations = ""
translations = ""
more_information = [ "https://invent.kde.org/plasma-mobile/plasma-settings",]
summary_source_url = "https://invent.kde.org/plasma-mobile/plasma-settings"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.mobile.plasmasettings"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "plasma-settings", "kde5-plasma-settings",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"

+++
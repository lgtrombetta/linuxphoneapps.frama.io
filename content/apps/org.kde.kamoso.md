+++
title = "Kamoso"
description = "Kamoso is a simple and friendly program to use your camera. Use it to take pictures and make videos to share."
aliases = []
date = 2020-02-06
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "gstreamer",]
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian",]
freedesktop_categories = [ "Qt", "KDE", "Graphics", "Photography",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/multimedia/kamoso"
homepage = "https://apps.kde.org/multimedia/org.kde.kamoso/"
bugtracker = "https://invent.kde.org/multimedia/kamoso/-/issues/"
donations = ""
translations = ""
more_information = [ "https://userbase.kde.org/Kamoso",]
summary_source_url = "https://apps.kde.org/multimedia/org.kde.kamoso/"
screenshots = [ "https://apps.kde.org/multimedia/org.kde.kamoso/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kamoso"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kamoso",]
appstream_xml_url = "https://invent.kde.org/multimedia/kamoso/-/raw/master/org.kde.kamoso.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++
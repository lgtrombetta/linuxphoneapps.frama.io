+++
title = "Headlines"
description = "A GTK4/libAdwaita Reddit client written in C++"
aliases = []
date = 2021-08-24
updated = 2023-03-04

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "caveman250",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Reddit",]
packaged_in = [ "AUR", "postmarketOS",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "News",]
programming_languages = [ "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://gitlab.com/caveman250/Headlines"
homepage = ""
bugtracker = "https://gitlab.com/caveman250/Headlines/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/caveman250/Headlines"
screenshots = [ "https://gitlab.com/caveman250/Headlines/-/tree/master/screenshots",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.gitlab.caveman250.headlines"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "headlines",]
appstream_xml_url = "https://gitlab.com/caveman250/Headlines/-/raw/master/io.gitlab.caveman250.headlines.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"

+++



### Notice

Great reddit client, sadly archived.
+++
title = "Drawing"
description = "This application is a simple image editor using Cairo and GdkPixbuf for basic drawing operations."
aliases = []
date = 2019-02-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Romain F. T.",]
categories = [ "drawing",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "AUR", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "Graphics", "RasterGraphics",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/maoschanz/drawing"
homepage = ""
bugtracker = "https://github.com/maoschanz/drawing/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/com.github.maoschanz.drawing/",]
summary_source_url = "https://github.com/maoschanz/drawing"
screenshots = [ "https://flathub.org/apps/details/com.github.maoschanz.drawing", "https://github.com/maoschanz/drawing/blob/master/docs/screenshots/0.6/librem_menu.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.maoschanz.drawing"
scale_to_fit = "com.github.maoschanz.drawing"
flathub = "https://flathub.org/apps/details/com.github.maoschanz.drawing"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "drawing",]
appstream_xml_url = "https://raw.githubusercontent.com/maoschanz/drawing/master/data/com.github.maoschanz.drawing.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++
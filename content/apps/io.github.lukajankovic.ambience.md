+++
title = "Ambience"
description = "Control LIFX lights"
aliases = []
date = 2021-01-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Luka Jankovic",]
categories = [ "smart home",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "lifxlan",]
services = [ "LIFX",]
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/LukaJankovic/Ambience"
homepage = ""
bugtracker = "https://github.com/LukaJankovic/Ambience/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/details/io.github.lukajankovic.ambience"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.lukajankovic.ambience"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/io.github.lukajankovic.ambience"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/LukaJankovic/Ambience/stable/data/io.github.lukajankovic.ambience.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Gtk (Handy) app to control LIFX smart lights using the lifxlan api. [Source](https://github.com/LukaJankovic/Ambience)
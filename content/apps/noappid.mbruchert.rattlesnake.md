+++
title = "Rattlensnake"
description = "Rattlesnake is a metronome app for mobile and desktop."
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "No license", "all rights reserved.",]
metadata_licenses = []
app_author = [ "mbruchert",]
categories = [ "musical tool",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "postmarketOS",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Music",]
programming_languages = [ "QML", "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/mbruchert/rattlesnake"
homepage = ""
bugtracker = "https://invent.kde.org/mbruchert/rattlesnake/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/mbruchert/rattlesnake"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = "Rattlesnake"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "rattlesnake",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

WIP, lacks desktop file and target for make install, can be used as a simple drumkit
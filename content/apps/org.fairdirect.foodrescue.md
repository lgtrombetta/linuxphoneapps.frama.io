+++
title = "Food Rescue App"
description = "Convergent mobile+desktop application to help decide if food is still edible."
aliases = []
date = 2020-08-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "Matthias Ansorg <matthias@ansorgs.de>",]
categories = [ "food rescue",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "QML", "C++", "XSLT",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/fairdirect/foodrescue-app"
homepage = ""
bugtracker = "https://github.com/fairdirect/foodrescue-app/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/fairdirect/foodrescue-app"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.fairdirect.foodrescue"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/fairdirect/foodrescue-app/master/metadata-kde.xml"
reported_by = "linmob"
updated_by = "script"

+++
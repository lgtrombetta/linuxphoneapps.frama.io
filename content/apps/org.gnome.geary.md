+++
title = "Geary"
description = "Geary is an email application built around conversations, for the GNOME 3 desktop."
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = []
app_author = [ "Geary Development Team",]
categories = [ "email",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Office", "Email",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/geary"
homepage = "https://wiki.gnome.org/Apps/Geary"
bugtracker = "https://gitlab.gnome.org/GNOME/geary/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/geary"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Geary"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnome.Geary"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "geary",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/geary/-/raw/mainline/desktop/org.gnome.Geary.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++
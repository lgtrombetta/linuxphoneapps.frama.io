+++
title = "SBB"
description = "A simple gtk app to get timetable information for swiss public transport."
aliases = []
date = 2021-05-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "chefe",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "transport.opendata.ch",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "make", "cargo",]

[extra]
repository = "https://github.com/chefe/sbb"
homepage = ""
bugtracker = "https://github.com/chefe/sbb/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/chefe/sbb"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1399016230380609539",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.chefe.sbb"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++
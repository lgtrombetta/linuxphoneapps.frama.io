+++
title = "Almond"
description = "The Almond Virtual Assistant, Linux desktop version"
aliases = []
date = 2021-05-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "Apache-2.0",]
metadata_licenses = []
app_author = [ "Stanford Open Virtual Assistant Lab",]
categories = [ "virtual assistant",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/stanford-oval/almond-gnome"
homepage = "https://almond.stanford.edu/"
bugtracker = "https://github.com/stanford-oval/almond-gnome/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/stanford-oval/almond-gnome"
screenshots = [ "https://fosstodon.org/@linmob/106313790975497432",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "edu.stanford.Almond.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/edu.stanford.Almond"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/stanford-oval/almond-gnome/release/data/edu.stanford.Almond.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Almond is a virtual assistant that lets you interact with your services and accounts in natural language, with flexibility and privacy. Almond draws its power from the crowdsourced Thingpedia, an open collection of Web and Internet of Things APIs. Anyone can contribute support for the favorite service, with few lines of code and a handful of natural language sentences. [Source](https://raw.githubusercontent.com/stanford-oval/almond-gnome/release/data/edu.stanford.Almond.appdata.xml.in)
+++
title = "Tinge"
description = "Tinge is a mobile first application for controlling Philips Hue lights on Linux"
aliases = []
date = 2021-05-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "micke",]
categories = [ "smart home",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "WxWidgets",]
backends = []
services = [ "Hue",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "custom",]

[extra]
repository = "https://code.smolnet.org/micke/tinge"
homepage = ""
bugtracker = "https://code.smolnet.org/micke/tinge/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://code.smolnet.org/micke/tinge"
screenshots = [ "https://code.smolnet.org/micke/tinge",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Could not test whether this actually works with Hue lights because I don't have any.
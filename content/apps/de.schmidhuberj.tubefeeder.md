+++
title = "Tubefeeder"
description = "A Youtube, Lbry and Peertube client made for the Pinephone"
aliases = []
date = 2021-03-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Julian Schmidhuber",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp",]
services = [ "YouTube", "lbry", "PeerTube",]
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Video", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://github.com/Tubefeeder/Tubefeeder"
homepage = "https://www.tubefeeder.de"
bugtracker = "https://github.com/Tubefeeder/Tubefeeder/issues"
donations = "https://www.tubefeeder.de/donate.html"
translations = ""
more_information = [ "https://www.tubefeeder.de/wiki/different-player.html",]
summary_source_url = "https://github.com/Tubefeeder/Tubefeeder"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/1.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/2.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/3.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/4.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/5.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.schmidhuberj.tubefeeder"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/de.schmidhuberj.tubefeeder"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "tubefeeder",]
appstream_xml_url = "https://raw.githubusercontent.com/Tubefeeder/Tubefeeder/master/data/de.schmidhuberj.tubefeeder.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Tubefeeder allows to watch Youtube, Lbry and Peertube videos on your Pinephone, including features like a subscription list, a filter list and a watch-later list.  Tubefeeder uses mpv to play the videos (Note that yt-dlp does to my knowledge not yet support Lbry, you need to use another video player). This is already included in Flatpak, install it otherwise. You can choose to use a different video player, for more information see [here](https://www.tubefeeder.de/wiki/different-player.html). Note that not all video players support every platform. [Source](https://raw.githubusercontent.com/Tubefeeder/Tubefeeder/master/data/de.schmidhuberj.tubefeeder.metainfo.xml)


### Notice

Make sure to read https://github.com/Schmiddiii/Tubefeeder/wiki. Ported to GTK4/libadwaita with release 1.6.0.
+++
title = "Messenger-GTK"
description = "An example GUI application for the GNUnet Messenger service using libgnunetchat"
aliases = []
date = 2021-12-24
updated = 2022-03-25

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "GNUnet e.V.",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "libgnunetchat",]
services = [ "GNUnet Messenger",]
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://git.gnunet.org/messenger-gtk.git/"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = [ "https://git.gnunet.org/messenger-gtk.git/tree/README.md", "https://www.reddit.com/r/pinephone/comments/rk5zyb/gnunet_messenger_for_mobile_linux/", "https://thejackimonster.blogspot.com/2022/02/gnunet-messenger-api-february.html", "https://thejackimonster.blogspot.com/2022/03/gnunet-messenger-api-march.html",]
summary_source_url = "https://gitlab.com/gnunet-messenger/messenger-gtk"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnunet.Messenger"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnunet.Messenger"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "messenger-gtk",]
appstream_xml_url = "https://git.gnunet.org/messenger-gtk.git/plain/resources/org.gnunet.Messenger.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"

+++


### Description

Messenger-GTK is a convergent GTK messaging application using the GNUnet Messenger service. The goal is to provide private and secure communication between any group of devices. [Source](https://git.gnunet.org/messenger-gtk.git/tree/README.md)
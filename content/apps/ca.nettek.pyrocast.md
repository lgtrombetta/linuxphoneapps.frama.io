+++
title = "Pyrocast"
description = "Another podcast app for Linux phones (e.g., Pinephone)"
aliases = []
date = 2021-05-29
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "jnetterf",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Feed", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://github.com/jocelyn-stericker/pyrocast"
homepage = ""
bugtracker = "https://github.com/jocelyn-stericker/pyrocast/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/jocelyn-stericker/pyrocast"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1398655122251649030",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "ca.nettek.pyrocast"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

WIP, buggy with a select feeds, no settings screen yet.
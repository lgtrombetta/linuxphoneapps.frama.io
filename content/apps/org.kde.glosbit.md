+++
title = "Glosbit"
description = "Translation tool powered by glosbe.com"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "dkardarakos",]
categories = [ "translation tool",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = [ "glosbe.com",]
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Network",]
programming_languages = [ "Javascript", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/dkardarakos/glosbit"
homepage = ""
bugtracker = "https://invent.kde.org/dkardarakos/glosbit/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/dkardarakos/glosbit"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.glosbit"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/dkardarakos/glosbit/-/raw/master/org.kde.glosbit.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++



### Notice

Last commit in April 2019.
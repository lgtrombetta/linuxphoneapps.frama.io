+++
title = "Clip"
description = "Video player and video collection manager"
aliases = []
date = 2020-12-19
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "maui",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = [ "mpv",]
services = []
packaged_in = [ "AUR", "postmarketOS",]
freedesktop_categories = [ "Qt", "Video", "Player",]
programming_languages = [ "QML", "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/clip"
homepage = "https://mauikit.org/apps/clip/"
bugtracker = "https://invent.kde.org/maui/clip/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/clip"
screenshots = [ "https://mauikit.org/wp-content/uploads/2021/08/clip.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.maui.clip"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "maui-clip",]
appstream_xml_url = "https://invent.kde.org/maui/clip/-/raw/master/org.kde.clip.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++
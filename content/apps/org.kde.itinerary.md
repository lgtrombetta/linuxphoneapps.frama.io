+++
title = "KDE Itinerary"
description = "Itinerary and boarding pass management application."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "pim",]
categories = [ "travel",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "Qt", "KDE", "Office", "Utility",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/pim/itinerary"
homepage = "https://apps.kde.org/itinerary/"
bugtracker = "https://invent.kde.org/pim/itinerary/-/issues/"
donations = ""
translations = ""
more_information = [ "https://www.volkerkrause.eu/2018/08/25/kde-itinerary-overview.html", "https://www.volkerkrause.eu/2019/01/26/kde-itinerary-december-january-2019.html", "https://community.kde.org/KDE_PIM/KDE_Itinerary",]
summary_source_url = "https://invent.kde.org/pim/itinerary"
screenshots = [ "https://apps.kde.org/itinerary/", "https://www.volkerkrause.eu/2018/08/25/kde-itinerary-overview.html",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.itinerary"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.kde.itinerary"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "itinerary", "kitinerary",]
appstream_xml_url = "https://invent.kde.org/pim/itinerary/-/raw/master/src/app/org.kde.itinerary.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++
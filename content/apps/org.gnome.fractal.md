+++
title = "Fractal"
description = "Matrix group messaging app"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "The Fractal Team",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Matrix",]
packaged_in = [ "postmarketOS", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/fractal"
homepage = "https://wiki.gnome.org/Apps/Fractal"
bugtracker = "https://gitlab.gnome.org/GNOME/fractal/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/fractal"
screenshots = [ "https://wiki.gnome.org/Apps/Fractal",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Fractal"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnome.Fractal"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "fractal",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/fractal/-/raw/main/data/org.gnome.Fractal.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Fractal is a Matrix messaging app for GNOME written in Rust. Its interface is optimized for collaboration in large groups, such as free software projects. [Source](https://gitlab.gnome.org/GNOME/fractal)
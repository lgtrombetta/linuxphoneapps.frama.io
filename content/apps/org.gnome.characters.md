+++
title = "GNOME Characters"
description = "Character map application"
aliases = []
date = 2021-03-31
updated = 2022-12-19

[taxonomies]
project_licenses = [ "BSD-3-Clause",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C", "JavaScript",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-characters"
homepage = "https://wiki.gnome.org/Apps/Characters"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-characters/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Characters/",]
summary_source_url = "https://wiki.gnome.org/Apps/Characters"
screenshots = [ "https://wiki.gnome.org/Apps/Characters",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Characters"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnome.Characters"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-characters",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-characters/-/raw/main/data/org.gnome.Characters.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Characters is a simple utility application to find and insert unusual characters.  It allows you to quickly find the character you are looking for by searching for keywords. [Source](https://wiki.gnome.org/Apps/Characters)


### Notice

Rating applies to version 40, previous releases may not work well.
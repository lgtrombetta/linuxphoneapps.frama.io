+++
title = "Shopping List"
description = "A shopping list application for GNU/Linux mobile devices"
aliases = []
date = 2022-05-31
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Cosmin Humeniuc",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/cosmin/shopping-list"
homepage = ""
bugtracker = "https://gitlab.gnome.org/cosmin/shopping-list/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/cosmin/shopping-list"
screenshots = [ "https://gitlab.gnome.org/cosmin/shopping-list/-/raw/master/data/screenshots/screenshot1.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "ro.hume.cosmin.ShoppingList"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/cosmin/shopping-list/-/raw/master/ro.hume.cosmin.ShoppingList.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/cosmin/shopping-list/-/raw/master/data/ro.hume.cosmin.ShoppingList.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Shopping List makes it easy to add items to a list, check the item off, and remove it from the list. [Source](https://gitlab.gnome.org/cosmin/shopping-list)
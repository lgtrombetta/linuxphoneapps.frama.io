+++
title = "Secrets"
description = "Secrets is a password manager which integrates perfectly with the GNOME desktop"
aliases = []
date = 2019-02-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Falk Alexander Seidl",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "pykeepass",]
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/secrets"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/secrets/-/issues/"
donations = ""
translations = "https://l10n.gnome.org/module/Secrets/"
more_information = [ "https://apps.gnome.org/app/org.gnome.World.Secrets/",]
summary_source_url = "https://gitlab.gnome.org/World/secrets"
screenshots = [ "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/browser.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/edit.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/entry.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/search.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/start.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/unlock.png", "https://matrix.to/#/!BSqRHgvCtIsGittkBG:talk.puri.sm/$154759674326879cFWXa:matrix.org?via=talk.puri.sm&via=matrix.org&via=disroot.org", "https://tchncs.de/_matrix/media/v1/download/matrix.org/VPBskuylCBwANlJJZsiFOwUx",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.World.Secrets"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnome.World.Secrets"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-passwordsafe", "secrets",]
appstream_xml_url = "https://gitlab.gnome.org/World/secrets/-/raw/master/data/org.gnome.World.Secrets.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Secrets is a password manager which integrates perfectly with the GNOME desktop and provides an easy and uncluttered interface for the management of password databases. Features: Create or import KeePass safes, Assign a color and additional attributes to entries, Add attachments to your encrypted database, Generate cryptographically strong passwords, Change the password or keyfile of your database, Quickly search your favorite entries, Automatic database lock during inactivity, Adaptive interface, Support for two-factor authentication. [Source](https://gitlab.gnome.org/World/secrets)


### Notice

Previously based on GTK3/libhandy and called GNOME PasswordSafe. Secrets additionally supports two-factor authentication.
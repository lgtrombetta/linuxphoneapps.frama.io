+++
title = "Baconer"
description = "Kirigami/ QT Reddit Client"
aliases = []
date = 2021-06-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "No license", "all rights reserved.",]
metadata_licenses = []
app_author = [ "tombebb",]
categories = [ "social media",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = [ "Reddit",]
packaged_in = []
freedesktop_categories = [ "Network", "Qt", "WebBrowser",]
programming_languages = [ "QML", "C++", "JavaScript",]
build_systems = [ "qmake",]

[extra]
repository = "https://github.com/TomBebb/Baconer"
homepage = ""
bugtracker = "https://github.com/TomBebb/Baconer/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/TomBebb/Baconer"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1403135178856349696",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

WIP, hard to rate: Fits the screen, but rendering of parts of the layout is bad on small screens and text disappears (see Screenshots).
+++
title = "Briar"
description = "Native GTK Briar client, written in Python, using the Briar Headless API"
aliases = []
date = 2020-12-01

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "The Briar Project",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "briar",]
packaged_in = [ "AUR", "Debian",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "P2P",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://code.briarproject.org/briar/briar-gtk"
homepage = "https://briarproject.org/"
bugtracker = ""
donations = ""
translations = ""
more_information = [ "https://nico.dorfbrunnen.eu/posts/2020/briar-beta/", "https://nico.dorfbrunnen.eu/posts/2020/briar-international/",]
summary_source_url = "https://code.briarproject.org/briar/briar-gtk"
screenshots = [ "https://twitter.com/linmobblog/status/1333868007395905536#m",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "app.briar.gtk"
scale_to_fit = "Briar"
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://code.briarproject.org/briar/briar-gtk/-/raw/main/app.briar.gtk.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "briar-gtk",]
appstream_xml_url = "https://code.briarproject.org/briar/briar-gtk/-/raw/main/briar-gtk/data/app.briar.gtk.metainfo.xml.in"
reported_by = "linmob"
updated_by = ""

+++



### Notice

Could not chat with Android client in initial test (2020/12/01), deprecated by https://code.briarproject.org/briar/briar-desktop/ (2021/09/21)
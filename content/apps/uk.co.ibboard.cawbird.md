+++
title = "Cawbird"
description = "A fork of the Corebird GTK Twitter client that continues to work with Twitter"
aliases = []
date = 2021-01-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "IBBoard",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = [ "twitter",]
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "News",]
programming_languages = [ "Vala", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/IBBoard/cawbird"
homepage = "https://ibboard.co.uk/cawbird/"
bugtracker = "https://github.com/IBBoard/cawbird/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/IBBoard/cawbird"
screenshots = [ "https://twitter.com/linmobblog/status/1345711646661107714#m",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "uk.co.ibboard.cawbird"
scale_to_fit = "cawbird"
flathub = "https://flathub.org/apps/details/uk.co.ibboard.cawbird"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "cawbird",]
appstream_xml_url = "https://raw.githubusercontent.com/IBBoard/cawbird/master/data/uk.co.ibboard.cawbird.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Perfect since release 1.3.1
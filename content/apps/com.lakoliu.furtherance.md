+++
title = "Furtherance"
description = "Track your time without being tracked"
aliases = []
date = 2022-03-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ricky Kresslein",]
categories = [ "productivity",]
mobile_compatibility = [ "4",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Clock",]
programming_languages = [ "Rust",]
build_systems = [ "meson", "cargo",]

[extra]
repository = "https://github.com/lakoliu/Furtherance"
homepage = "https://furtherance.app/"
bugtracker = "https://github.com/lakoliu/Furtherance/issues/"
donations = "https://furtherance.app/"
translations = ""
more_information = []
summary_source_url = "https://github.com/lakoliu/Furtherance"
screenshots = [ "https://raw.githubusercontent.com/lakoliu/Furtherance/main/data/screenshots/furtherance-screenshot-edit-task.png", "https://raw.githubusercontent.com/lakoliu/Furtherance/main/data/screenshots/furtherance-screenshot-main.png", "https://raw.githubusercontent.com/lakoliu/Furtherance/main/data/screenshots/furtherance-screenshot-settings.png", "https://raw.githubusercontent.com/lakoliu/Furtherance/main/data/screenshots/furtherance-screenshot-task-details.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.lakoliu.Furtherance"
scale_to_fit = "com.lakoliu.Furtherance"
flathub = "https://flathub.org/apps/details/com.lakoliu.Furtherance"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "furtherance",]
appstream_xml_url = "https://raw.githubusercontent.com/lakoliu/Furtherance/main/data/com.lakoliu.Furtherance.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Furtherance is a time tracking app written in Rust with GTK 4. It allows you to track time spent on different activities without worrying about your data being captured and sold. [Source](https://github.com/lakoliu/Furtherance)


### Notice

Was narrow enough for a moment - as of 1.1.2 scalte-to-fit is necessary again.
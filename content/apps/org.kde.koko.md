+++
title = "Koko"
description = "An Image Gallery application"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = []
app_author = [ "graphics",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Flathub",]
freedesktop_categories = [ "Qt", "KDE", "Graphics", "Viewer",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/graphics/koko"
homepage = "https://apps.kde.org/koko/"
bugtracker = "https://invent.kde.org/graphics/koko/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://phabricator.kde.org/project/profile/235/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.koko"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.kde.koko"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "koko",]
appstream_xml_url = "https://invent.kde.org/graphics/koko/-/raw/master/org.kde.koko.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++
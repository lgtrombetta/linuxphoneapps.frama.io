+++
title = "Planner"
description = "Never worry about forgetting things again"
aliases = []
date = 2020-08-26
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Alain M.",]
categories = [ "productivity",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "GTK3",]
backends = []
services = [ "Todoist",]
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "GTK", "Office", "Project Management",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/alainm23/planner"
homepage = "https://planner-todo.web.app/"
bugtracker = "https://github.com/alainm23/planner/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://planner-todo.web.app/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.alainm23.planner"
scale_to_fit = "com.github.alainm23.planner"
flathub = "https://flathub.org/apps/details/com.github.alainm23.planner"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "elementary-planner",]
appstream_xml_url = "https://raw.githubusercontent.com/alainm23/planner/master/data/com.github.alainm23.planner.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

works after scale-to-fit
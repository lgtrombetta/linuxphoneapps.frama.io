+++
title = "MiTubo"
description = "A native video player for YouTube videos."
aliases = []
date = 2021-09-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "mardy",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = [ "gstreamer", "youtube-dl",]
services = [ "YouTube",]
packaged_in = []
freedesktop_categories = [ "Qt", "Network", "Feed", "AudioVideo", "Player",]
programming_languages = [ "C++", "QML",]
build_systems = [ "qbs",]

[extra]
repository = "https://gitlab.com/mardy/mitubo"
homepage = ""
bugtracker = "https://gitlab.com/mardy/mitubo/-/issues/"
donations = "https://gitlab.com/mardy/mitubo#contribute"
translations = ""
more_information = [ "https://www.mardy.it/blog/2021/09/mitubo-03-brings-basic-rss-support.html", "https://open-store.io/app/it.mardy.mitubo", "https://www.mardy.it/blog/2022/04/mitubo-update-search-channels-watch-later-queue.html",]
summary_source_url = "https://gitlab.com/mardy/mitubo"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "it.mardy.mitubo"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/mardy/mitubo/-/raw/master/data/it.mardy.mitubo.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Originally for Ubuntu Touch, this app works on other distributions too.
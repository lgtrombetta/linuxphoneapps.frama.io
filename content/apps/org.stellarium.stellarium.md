+++
title = "Stellarium"
description = "Stellarium is a free GPL software which renders realistic skies in real time with OpenGL."
aliases = []
date = 2020-10-27
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "Stellarium team",]
categories = [ "entertainment",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "Qt", "Education", "Astronomy",]
programming_languages = [ "C++", "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/Stellarium/stellarium"
homepage = "https://stellarium.org/"
bugtracker = "https://github.com/Stellarium/stellarium/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Stellarium/stellarium"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.stellarium.Stellarium"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.stellarium.Stellarium"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "stellarium",]
appstream_xml_url = "https://raw.githubusercontent.com/Stellarium/stellarium/master/data/org.stellarium.Stellarium.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++
+++
title = "Vehicle Voyage"
description = "Cross platform app to track vehicle history."
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "eyecreate",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "QML", "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://git.eyecreate.org/eyecreate/vehicle-voyage"
homepage = ""
bugtracker = "https://git.eyecreate.org/eyecreate/vehicle-voyage/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.eyecreate.org/eyecreate/vehicle-voyage"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.eyecreate.vehiclevoyage"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.eyecreate.vehiclevoyage"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://git.eyecreate.org/eyecreate/vehicle-voyage/-/raw/master/packaging/org.eyecreate.vehiclevoyage.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Cross platform app to track vehicle history. [Source](https://git.eyecreate.org/eyecreate/vehicle-voyage)
+++
title = "PlasmaTube"
description = "Kirigami YouTube video player based on QtMultimedia and youtube-dl"
aliases = []
date = 2019-04-16
updated = 2023-02-21

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Plasma Mobile Developers",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "libmpv", "youtube-dl",]
services = [ "invidious API", "YouTube",]
packaged_in = [ "AUR", "postmarketOS", "Flathub",]
freedesktop_categories = [ "Qt", "KDE", "Network", "AudioVideo", "Player",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/multimedia/plasmatube"
homepage = "https://apps.kde.org/plasmatube/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=PlasmaTube"
donations = "https://kde.org/community/donations/?app=org.kde.plasmatube"
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/multimedia/plasmatube"
screenshots = [ "https://cdn.kde.org/screenshots/plasmatube/plasmatube.png", "https://share.kaidan.im/lnj/B2ywsz5K2DtXLspZsuQz/Screenshot_20190320_221358.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.plasmatube"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.kde.plasmatube"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "plasmatube",]
appstream_xml_url = "https://invent.kde.org/multimedia/plasmatube/-/raw/master/org.kde.plasmatube.appdata.xml"
reported_by = "cahfofpai"
updated_by = "linmob"

+++
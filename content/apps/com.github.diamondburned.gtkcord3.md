+++
title = "gtkcord3"
description = "A lightweight Discord client which uses GTK3 for the user interface."
aliases = []
date = 2020-08-26
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "diamondburned",]
categories = [ "chat",]
mobile_compatibility = [ "4",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Discord",]
packaged_in = []
freedesktop_categories = [ "GTK", "Network", "Chat",]
programming_languages = [ "Go",]
build_systems = [ "go",]

[extra]
repository = "https://github.com/diamondburned/gtkcord3"
homepage = ""
bugtracker = "https://github.com/diamondburned/gtkcord3/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/diamondburned/gtkcord3"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.diamondburned.gtkcord3"
scale_to_fit = "com.github.diamondburned.gtkcord3"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gtkcord",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Chat screens don’t scale properly without scale-to-fit, for abandonned preparations for flathub publication see issue #80.
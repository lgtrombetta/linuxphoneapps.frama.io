+++
title = "Keysmith"
description = "OTP client for Plasma Mobile and Desktop"
aliases = []
date = 2019-04-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "multi-factor authentication",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Flathub",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "C++", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/keysmith"
homepage = "https://apps.kde.org/keysmith/"
bugtracker = "https://invent.kde.org/utilities/keysmith/-/issues/"
donations = ""
translations = ""
more_information = [ "https://blog.bshah.in/2019/12/18/keysmith-v0-1-release/",]
summary_source_url = "https://invent.kde.org/utilities/keysmith"
screenshots = [ "https://invent.kde.org/utilities/keysmith/-/merge_requests/30",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.keysmith"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.kde.keysmith"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "keysmith",]
appstream_xml_url = "https://invent.kde.org/utilities/keysmith/-/raw/master/org.kde.keysmith.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

It uses the oath-toolkit provided library liboath to generate the 2FA codes, both TOTP and HOTP based. Currently it is largely untested. From initial rough testing it seems that auto-refreshing of code is not working. Also button to refresh token for HOTP is also dummy at moment. [Source](https://invent.kde.org/utilities/keysmith)
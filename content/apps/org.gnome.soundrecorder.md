+++
title = "GNOME Sound Recorder"
description = "A simple and modern sound recorder"
aliases = []
date = 2021-03-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "audio recorder",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Recorder",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-sound-recorder"
homepage = "https://wiki.gnome.org/Apps/SoundRecorder"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-sound-recorder/-/issues/"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = []
summary_source_url = "https://wiki.gnome.org/Apps/SoundRecorder"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.SoundRecorder"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnome.SoundRecorder"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-sound-recorder",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-sound-recorder/-/raw/master/data/org.gnome.SoundRecorder.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

GTK4/libadwaita since release 42.
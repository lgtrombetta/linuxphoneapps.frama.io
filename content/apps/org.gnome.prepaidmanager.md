+++
title = "Prepaid Manager"
description = "Prepaid-manager-applet (ppm) is an applet for the GNOME Desktop that allows you to check and top up the balance of GSM mobile prepaid SIM cards."
aliases = []
date = 2020-10-15

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "cgit",]
categories = [ "mobile service utility",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TelephonyTools",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://git.sigxcpu.org/cgit/ppm"
homepage = "https://honk.sigxcpu.org/piki/projects/ppm/#index2h3"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://honk.sigxcpu.org/piki/projects/ppm/#index2h3"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.PrepaidManager"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "prepaid-manager-applet",]
appstream_xml_url = "https://git.sigxcpu.org/cgit/ppm/plain/data/org.gnome.PrepaidManager.appdata.xml.in"
reported_by = "linmob"
updated_by = ""

+++
+++
title = "Phosh Look"
description = "Phosh look manages ~/.config/gtk-3.0 with preset themes"
aliases = []
date = 2021-06-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "david.hamner",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "custom",]

[extra]
repository = "https://source.puri.sm/david.hamner/phosh_look"
homepage = ""
bugtracker = "https://source.puri.sm/david.hamner/phosh_look/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/david.hamner/phosh_look"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Hardcoded paths limit this app to PureOS or similar systems with a user named purism right now.
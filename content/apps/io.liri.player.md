+++
title = "Liri Player"
description = "Cross-platform media player"
aliases = []
date = 2020-11-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "The Liri developers",]
categories = [ "video player",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "AudioVideo", "Audio", "Video", "Player",]
programming_languages = [ "QML", "C++",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/lirios/player"
homepage = ""
bugtracker = "https://github.com/lirios/player/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/lirios/player"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.liri.Player"
scale_to_fit = "io.liri.Player"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/lirios/player/develop/data/appdata/io.liri.Player.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Fine with scale-to-fit
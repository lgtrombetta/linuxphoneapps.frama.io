+++
title = "Ear Tag"
description = "Small and simple music tag editor that doesn't try to manage your entire library"
aliases = []
date = 2022-09-08
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "knuxify",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/knuxify/eartag"
homepage = ""
bugtracker = "https://github.com/knuxify/eartag/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/knuxify/eartag"
screenshots = [ "https://raw.githubusercontent.com/knuxify/eartag/main/data/screenshot-scaled.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "app.drey.EarTag"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/app.drey.EarTag"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "eartag",]
appstream_xml_url = "https://raw.githubusercontent.com/knuxify/eartag/main/data/app.drey.EarTag.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A lot of music tag editors are made to apply changes to entire music libraries. They require you to set up a music folder, etc. This is convenient when you want to keep your entire library in check, but sometimes you just need to edit one file's data without any of the additional hassle.  Thus, Ear Tag was made to be a simple tag editor that can edit singular files as needed.  (Additionally, due to its compact design, it can be used on mobile Linux devices.) [Source](https://github.com/knuxify/eartag)
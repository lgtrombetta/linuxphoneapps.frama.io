+++
title = "Millipixels"
description = "A camera application for the Librem 5."
aliases = []
date = 2021-12-14
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Purism",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = [ "libcamera",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Photography",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://source.puri.sm/Librem5/millipixels"
homepage = ""
bugtracker = "https://source.puri.sm/Librem5/millipixels/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/Librem5/millipixels"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "sm.puri.Millipixels"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "millipixels",]
appstream_xml_url = "https://source.puri.sm/Librem5/millipixels/-/raw/master/data/sm.puri.Millipixels.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Millipixel's goals is to exercise libcamera APIs to help it support the capabilities of the Librem 5 phone. It's meant to stay the supported Librem 5 application until a suitable replacement arrives. It's forked off Megapixels. [Source](https://source.puri.sm/Librem5/millipixels)


### Notice

Megapixels (GTK3 branch) fork by Purism for the Librem 5. Uses libcamera.
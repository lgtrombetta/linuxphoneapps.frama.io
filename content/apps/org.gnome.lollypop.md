+++
title = "Lollypop"
description = "Lollypop is a modern music player for GNOME"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Music", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/lollypop"
homepage = "https://wiki.gnome.org/Apps/Lollypop"
bugtracker = "https://gitlab.gnome.org/World/lollypop/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://wiki.gnome.org/Apps/Lollypop"
screenshots = [ "https://wiki.gnome.org/Apps/Lollypop",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Lollypop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/org.gnome.Lollypop"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "lollypop", "lollypop-next", "lollypop-stable",]
appstream_xml_url = "https://gitlab.gnome.org/World/lollypop/-/raw/master/data/org.gnome.Lollypop.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++
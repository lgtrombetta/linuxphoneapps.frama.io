+++
title = "Markets"
description = "A stock, currency and cryptocurrency tracker"
aliases = []
date = 2020-08-25
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "bitstower",]
categories = [ "stocks",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "AUR", "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "News",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/tomasz-oponowicz/markets"
homepage = ""
bugtracker = "https://github.com/tomasz-oponowicz/markets/issues"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/com.bitstower.Markets/",]
summary_source_url = "https://flathub.org/apps/details/com.bitstower.Markets"
screenshots = [ "https://github.com/tomasz-oponowicz/markets",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.bitstower.Markets"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/com.bitstower.Markets"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "markets",]
appstream_xml_url = "https://raw.githubusercontent.com/bitstower/markets/master/data/com.bitstower.Markets.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

The Markets application delivers financial data to your fingertips. Track stocks, currencies and cryptocurrencies. Stay on top of the market and never miss an investment opportunity! [Source](https://github.com/tomasz-oponowicz/markets)


### Notice

Sadly the repo is now a public archive.
+++
title = "Hello World 👋"
date = 2022-03-28T20:00:00+02:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["linmob"]

[extra]
lead = "LinuxPhoneApps.org has been in planning and development for a long, long time. Today, we're finally launching, because it's better to launch poorly than never."
images = []
+++


Note: It's not all ready, and things aren't super great. But, after such a long process, launching in a "not fully baked" state feels a lot better than launching never.

### What's missing?

* __Individual app listings__ are here, but they aren't quite what we were aiming for:
  * We don't have screenshots yet, soon we might incorporate some for images uploaded somewhere else. Hosting too many images in a git repo is not a good idea, and it would make things more complicated - this way, every listing is just a markdown file that follows a certain logic.
  * The markdown files can't be edited directly yet.
  * We can't reuse contents from structured appstream metadata yet.
  * Other considered features, such as some kind of user comment system, have not been tackled at all yet. Merge requests for a system that requires some kind of authentication, be it ActivityPub, Matrix or else, are very welcome, though!
* __No new representation of the Games List.__ It's going to land, once apps are "done".
* __Docs/Resources.__ We have some, but especially developer resources aren't more than a bunch of links, which makes sense given that I am not a developer, despite the [pythonic mess](https://framagit.org/linuxphoneapps/appscsv2tomlmd) I've created to get this effort to this point.

### Why are we launching anyway?

There's no regression compared the previous state: The original JavaScript heavy lists are still here:[^1]
* [Apps](https://linuxphoneapps.org/lists/apps-classic) (dubbed "Classical App List" 😉)
* [Games](https://linuxphoneapps.org/lists/games),
* [Archive](https://linuxphoneapps.org/lists/archive), which has been partly re-integrated into apps.csv (with the status set to archived or inactive) and now only lists two apps for which re-integration did not make any sense.

We're also not loosing any Git history, as the original .csv lists are not only meant to stay here, but their [repo](https://linmobapps.frama.io) is still intact and a part of this project. It really should be moved to be part of the same organization, but in order to make this transition easier it will remain where it is. Also, a single source of truth, such as apps.csv, has its benefits - it can be reused in [third party](https://github.com/Speiburger/LINMOBappsBrowser) [projects](https://framagit.org/1peter10/simple-applist) easily. Yes, CSV is ugly, but as long as there's no replacement (e.g. in XML), we'll have to keep it around.

Also, with all the caveats applied, this should be a lot better then what we're had before, for repology integration alone!

### What's next?

Aside from what's in the [issue](https://framagit.org/groups/linuxphoneapps/-/issues) [trackers](https://framagit.org/linmobapps/linmobaapps.frama.io/-/issues), there's a lot of design work that will need to happen. Several overviews are far from perfect, and I am not even happy with how the app listings are looking right now. They need to be reordered and polished, which will happen over time.

There's a lot of work ahead, but hey, at least we're launched now! 🥳🎉


[^1]: They work exactly the same way as they always did – except for some unfixed regressions when dark mode is active.

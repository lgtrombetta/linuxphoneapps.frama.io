+++
title = "MartianRun"
description = "A 2D running game built with libGDX."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "Apache-2.0",]
categories = [ "game",]
mobile_compatibility = [ "A",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/wmora/martianrun"
homepage = ""
more_information = []
summary_source_url = "https://github.com/wmora/martianrun"
screenshots = [ "https://play.google.com/store/apps/details?id=com.gamestudio24.cityescape.android&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1",]
screenshots_img = []
app_id = "com.gamestudio24.martianrun"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
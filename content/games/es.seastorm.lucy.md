+++
title = "Lucy"
description = "Lucy's family has been abducted by Aliens! Help Lucy to reach the four UFOS and rescue her family."
aliases = []
date = 2019-03-17

[taxonomies]
project_licenses = [ "Apache-2.0",]
categories = [ "game",]
mobile_compatibility = [ "A",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/pabloalba/lucy/"
homepage = ""
more_information = []
summary_source_url = "https://play.google.com/store/apps/details?id=es.seastorm.lucy"
screenshots = [ "https://play.google.com/store/apps/details?id=es.seastorm.lucy",]
screenshots_img = []
app_id = "es.seastorm.lucy"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
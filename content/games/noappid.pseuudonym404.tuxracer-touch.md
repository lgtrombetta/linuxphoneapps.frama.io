+++
title = "TuxRacer-Touch"
description = "Ubuntu touch tux racer port"
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/pseuudonym404/tuxracer-touch"
homepage = ""
more_information = []
summary_source_url = "https://github.com/pseuudonym404/tuxracer-touch"
screenshots = [ "https://uappexplorer.com/app/tuxracer.lb",]
screenshots_img = []
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
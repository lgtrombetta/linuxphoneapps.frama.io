+++
title = "Flappy-gnome"
description = "Flappy GNOME is a side-scrolling game development tutorial using Vala and GTK+."
aliases = []
date = 2019-03-04

[taxonomies]
project_licenses = [ "not specified",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://gitlab.gnome.org/albfan/flappy-gnome-tutorial"
homepage = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/albfan/flappy-gnome-tutorial"
screenshots = [ "https://gitlab.gnome.org/albfan/flappy-gnome-tutorial",]
screenshots_img = []
app_id = "org.gnome.Flappy"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
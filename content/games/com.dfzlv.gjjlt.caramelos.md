+++
title = "Caramelos"
description = "Caramelos is an Android Game made with Libgdx"
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "MIT",]
categories = [ "game",]
mobile_compatibility = [ "A",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/luarca84/Caramelos"
homepage = ""
more_information = []
summary_source_url = "https://github.com/luarca84/Caramelos"
screenshots = [ "https://play.google.com/store/apps/details?id=com.dfzlv.gjjlt.caramelos",]
screenshots_img = []
app_id = "com.dfzlv.gjjlt.caramelos"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
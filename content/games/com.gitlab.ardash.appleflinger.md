+++
title = "Apple Flinger"
description = "Funny single- and multiplayer game for Android - Use a slingshot to shoot with apples. Very challenging and addictive."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
categories = [ "game",]
mobile_compatibility = [ "A",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://gitlab.com/ar-/apple-flinger"
homepage = ""
more_information = []
summary_source_url = "https://gitlab.com/ar-/apple-flinger"
screenshots = []
screenshots_img = []
app_id = "com.gitlab.ardash.appleflinger"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
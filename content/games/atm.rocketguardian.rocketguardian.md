+++
title = "Rocket Guardian"
description = "Rocket Guardian is a android game about a guardian that must protect a city from the falling zombies."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://gitlab.com/atorresm/rocket-guardian"
homepage = ""
more_information = []
summary_source_url = "https://gitlab.com/atorresm/rocket-guardian"
screenshots = []
screenshots_img = []
app_id = "atm.rocketguardian.RocketGuardian"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
+++
title = "PixelWheels"
description = "Pixel Wheels is a top-down racing game for PC (Linux, Mac, Windows) and Android."
aliases = []
date = 2019-02-16

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "A",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "✅"
repository = "https://github.com/agateau/pixelwheels"
homepage = "https://agateau.com/projects/pixelwheels/"
more_information = []
summary_source_url = "http://agateau.com/projects/pixelwheels/"
screenshots = []
screenshots_img = []
app_id = "com.agateau.tinywheels.android"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++


### Description

Race for the first place on various tracks. Pick up bonuses to boost your position or slow down competitors!" [Source](http://agateau.com/projects/pixelwheels/)


### Notice

Patched to run by Moxvallix: https://github.com/moxvallix/LinMobGames
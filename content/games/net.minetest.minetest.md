+++
title = "Minetest"
description = "Minetest, an open source infinite-world block sandbox game engine with support for survival and crafting."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later AND CC-BY-SA-3.0 AND MIT AND Apache-2.0",]
categories = [ "game",]
mobile_compatibility = [ "A",]
frameworks = []
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/minetest/minetest"
homepage = "https://www.minetest.net/"
more_information = []
summary_source_url = "https://github.com/minetest/minetest#compiling"
screenshots = []
screenshots_img = []
app_id = "net.minetest.Minetest"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/net.minetest.Minetest"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "minetest",]
appstream_xml_url = "https://raw.githubusercontent.com/minetest/minetest/master/misc/net.minetest.minetest.appdata.xml"

+++



### Notice

Once this pull request is merged, it will have touch screen support. https://github.com/minetest/minetest/pull/10729
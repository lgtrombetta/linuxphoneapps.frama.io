def get_recursive(obj, path, default=None):
    if isinstance(path, str):
        path = path.split(".")

    if len(path) == 0:
        return obj

    key, *path = path
    if key not in obj:
        return default

    return get_recursive(obj[key], path, default)


def set_recursive(obj, path, value):
    if isinstance(path, str):
        path = path.split(".")

    if len(path) == 0:
        raise KeyError("Empty path")

    key, *path = path
    if len(path) > 0:
        if key not in obj:
            obj[key] = {}
        set_recursive(obj[key], path, value)
    else:
        obj[key] = value

#!/usr/bin/python3
from itertools import chain
import asyncio
import datetime
import io
import pathlib
import sys
import urllib.parse

import aiofiles
import frontmatter
import httpx
from PIL import Image

import utils


def is_link(url):
    return url.strip().startswith("http")


async def check_link_reachable(client, url):
    try:
        response = await client.head(url, follow_redirects=False)
        code = response.status_code
        reason = response.reason_phrase.capitalize()
        if code == 200:
            return None
        elif code == 301 or code == 308:
            location = this_location = urllib.parse.urljoin(url, response.headers["Location"])
            message = f"Permanent redirect to {location}"
            # recursive resolution
            if (result := await check_link_reachable(client, location)) is not None:  # reachable
                if result[0] is not None:  # redirect url
                    location, message = result
                    message += f" via {this_location}"
            return (location, message)
        elif code == 302 or code == 307:
            location = urllib.parse.urljoin(url, response.headers["Location"])
            return (None, f"Temporary redirect ({code}) to {location}")
        return (None, f"{reason} ({code})")
    except Exception as e:
        return (None, f"Error occurred checking {url}: {e}")


async def check_link_https(client, url):
    if url.startswith("https"):
        return None

    suggestion = url.replace("http", "https")
    if await check_link_reachable(client, suggestion) is not None:  # not reachable
        suggestion = None

    return (suggestion, "Not HTTPS!")


async def check_field(client, field):
    links = field
    if not isinstance(links, list):
        links = [links]
    links = [link for link in links if is_link(link)]

    checkers = [check_link_reachable, check_link_https]
    for i, _ in enumerate(links):
        for checker in checkers:
            result = await checker(client, links[i])
            if not result:  # reachable
                continue
            found_url, result_message = result
            print(f"{links[i]}: {result_message}", file=sys.stderr)
            if found_url:
                links[i] = found_url
    return links if not isinstance(field, list) or len(links) < 1 else links[0]


async def check_screenshot(client, image_url):
    try:
        async with client.stream("GET", image_url) as response:
            content_type = response.headers["content-type"]
            if not content_type.startswith("image/"):
                return f'{image_url}: Invalid image type "{content_type}"'

            image = Image.open(io.BytesIO(await response.aread()))
            width, height = image.size
            if width > height:
                return f"{image_url}: Landscape image with dimensions {width}x{height} detected but expecting mobile images to be portrait ones"
        return None
    except Exception as e:
        return f"{image_url}: Error occurred: {e}"


# check links for error
async def check(client, item, update=False, item_root=None):
    if item_root is None:
        item_root = item

    found = False
    for key in item:
        if isinstance(item[key], dict):
            found |= await check(client, item[key], update, item_root)
            continue

        field = await check_field(client, item[key])
        if field != item[key]:  # found
            found = True
            if update:
                item[key] = field
                utils.set_recursive(item_root, "updated", str(datetime.date.today()))
                utils.set_recursive(item_root, "extra.updated_by", "script")

    screenshots = chain(filter(None, utils.get_recursive(item, "extra.screenshots", [])), filter(None, utils.get_recursive(item, "extra.screenshots_img", [])))
    for screenshot in screenshots:
        screenshot_error = await check_screenshot(client, screenshot)
        if screenshot_error is not None:
            print(screenshot_error, file=sys.stderr)

    return found


async def check_file(client, filename, update=False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    found = await check(client, doc.metadata, update)

    if found and update:
        print(f"Writing changes to {filename}")
        async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
            await f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


async def run(folder, update=False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        for filename in folder.glob("**/*.md"):
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(client, filename, update)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix FOLDER")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_folder = pathlib.Path(sys.argv[2])
    found = await run(apps_folder, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_folder}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
